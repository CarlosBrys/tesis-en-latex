# Elaboración de Tesis en LaTeX

Plantilla para elaborar una memoria de Tesis en LaTeX

# Motivación

A lo largo de mi vida académica escribí informes, manuales, artículos científicos, libros y también tesis.

Tiempo atrás, un procesador de textos (que pensé solucionaba todos mis problemas) era más que suficiente para producir los documentos que necesitaba. Hasta que llegó un momento en que tuve que escribir mi tesis de maestría y lidiar con fórmulas, cientos de imágenes, referencias cruzadas, bibliografías, índices, enlaces y documentos enormes. Y rogar que no se rompa nada...

Fue en el año 2001 que conocí al Dr. Enrique Castillo Ron de la Universidad de Cantabria, quien me hizo cambiar mi paradigma de raíz, y masticar mi orgullo basado en todo lo que expertamente sabía hacer con el Writer de LibreOffice, al ver cómo LaTeX solucionaba los complejos problemas de edición, y el resultado final nunca podría ser igualado por un procesador de textos.

La mayoría, sino todos, los libros de LaTeX se enfocan en explicarlo como un lenguaje para el maquetado de documentos, haciendo una pormenorizada, extensa y poco atractiva descripción del qué es. Como si para poder montar una bicicleta usted debería conocer previamente todos y cada uno de los elementos que la componen. Yo pretendo aquí resumir el cómo se usa, y que usted salga pedaleando
cuanto antes.

Con este documento aspiro compartirle mi conocimiento, fruto de mi experiencia con LaTeX, simplificando el proceso de crear un libro, sorteando la tediosa tarea de leer la bibliografía de LaTeX, la que no está escrita para quien necesita producir inmediatamente, agobiado por el estrés de escribir una tesis que tenga una estructura académica formal, y se vea estéticamente agradable.

Al final, creo que hay una frase que simplifica lo que significa la experiencia de usar LaTeX:

“Al principio lo odiarás, pero terminarás amándolo”

## Autor
Carlos Brys.

## Licencia
Creative Commons BY-SA-NC.

